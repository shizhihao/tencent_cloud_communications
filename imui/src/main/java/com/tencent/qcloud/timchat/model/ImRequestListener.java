package com.tencent.qcloud.timchat.model;

import java.util.ArrayList;

public abstract class ImRequestListener {
    public ArrayList<String> requestUserNames;

    public abstract void success(ArrayList<ImChatUser> users);

    public abstract void error(Object result);
}
