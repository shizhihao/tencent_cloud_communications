package com.tencent.qcloud.timchat;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

import com.tencent.imsdk.TIMConversationType;
import com.tencent.imsdk.TIMGroupReceiveMessageOpt;
import com.tencent.imsdk.TIMManager;
import com.tencent.imsdk.TIMOfflinePushListener;
import com.tencent.imsdk.TIMOfflinePushNotification;
import com.tencent.qalsdk.sdk.MsfSdkUtils;
import com.tencent.qcloud.timchat.model.ImChatUser;
import com.tencent.qcloud.timchat.model.ImRequestListener;
import com.tencent.qcloud.timchat.model.UserInfoProvider;
import com.tencent.qcloud.timchat.ui.ChatActivity;
import com.tencent.qcloud.timchat.utils.Foreground;

import java.util.ArrayList;


/**
 * 全局Application
 */
public class ImInit {
    private Context context;
    private UserInfoProvider userInfoProvider;
    private String newChatActivity;

    private static class ImInitInstance {
        private static final ImInit imInit = new ImInit();
    }

    public static ImInit getInstance() {
        return ImInitInstance.imInit;
    }

    public static void init(final Application application) {
        getInstance().context = application;

        Foreground.init(application);
        if (MsfSdkUtils.isMainProcess(application)) {
            try {
                final ApplicationInfo appInfo = application.getPackageManager().getApplicationInfo(application.getPackageName(), PackageManager.GET_META_DATA);
                TIMManager.getInstance().setOfflinePushListener(new TIMOfflinePushListener() {
                    @Override
                    public void handleNotification(TIMOfflinePushNotification notification) {
                        if (notification.getGroupReceiveMsgOpt() == TIMGroupReceiveMessageOpt.ReceiveAndNotify) {
                            //消息被设置为需要提醒
                            notification.doNotify(application, appInfo.icon);
                        }
                    }
                });
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public void setUserInfoProvider(UserInfoProvider provider) {
        userInfoProvider = provider;
    }

    public void setNewChatActivity(String newChatActivity) {
        this.newChatActivity = newChatActivity;
    }

    public static void navToChat(Context context, String identify, TIMConversationType type) {
        Intent intent = new Intent(context, ChatActivity.class);
        if (getInstance().newChatActivity != null) {
            intent.setClassName(context, getInstance().newChatActivity);
        }
        intent.putExtra("identify", identify);
        intent.putExtra("type", type);
        context.startActivity(intent);
    }

    public static ImChatUser getImChatUser(String userName) {
        if (getInstance().userInfoProvider != null)
            return getInstance().userInfoProvider.getUserInfo(userName);
        return null;
    }

    public static void requestImChatUser(ArrayList<String> userName, ImRequestListener listener) {
        if (getInstance().userInfoProvider != null)
            getInstance().userInfoProvider.requestUserInfo(userName, listener);
        listener.error(null);
    }

    public static Context getContext() {
        return getInstance().context;
    }
}
