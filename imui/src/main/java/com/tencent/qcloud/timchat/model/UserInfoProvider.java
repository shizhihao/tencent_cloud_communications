package com.tencent.qcloud.timchat.model;

import java.util.ArrayList;
import java.util.HashSet;

public abstract class UserInfoProvider {
    public static final String USER_SELF = "##user_self";
    public ArrayList<String> requestNames;
    public HashSet<String> utilSet = new HashSet<>();

    public ArrayList<String> getRequestNames() {
        if (requestNames == null)
            requestNames = new ArrayList<>();
        return requestNames;
    }

    public void requestUserInfo(ArrayList<String> userName, ImRequestListener listener) {
        if (userName != null && !userName.isEmpty()) {
            // 判断待请求数据
            if (getRequestNames().isEmpty()) {
                // 整合数据，发起请求
                getRequestNames().addAll(userName);
                listener.requestUserNames = userName;
                getUserInfo(userName, listener);
            } else {
                utilSet.clear();
                utilSet.addAll(requestNames);
                utilSet.addAll(userName);

                if (requestNames.size() < utilSet.size()) {
                    userName.clear();

                    int beforeSize = requestNames.size();
                    requestNames.clear();
                    requestNames.addAll(utilSet);

                    for (int i = beforeSize; i < requestNames.size(); i++) {
                        userName.add(requestNames.get(i));
                    }
                    listener.requestUserNames = userName;
                    getUserInfo(userName, listener);
                } else {
                    listener.error(null);
                }
            }
        } else {
            listener.error(null);
        }
    }

    public void requestSuccess(ImRequestListener listener) {
        if (!getRequestNames().isEmpty() && listener.requestUserNames != null && !listener.requestUserNames.isEmpty()) {
            requestNames.removeAll(listener.requestUserNames);
        }
    }

    public abstract ImChatUser getUserInfo(String userName);

    public abstract void getUserInfo(ArrayList<String> userName, ImRequestListener listener);
}
