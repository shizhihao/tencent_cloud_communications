package com.tencent.qcloud.timchat.adapters;

import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseViewHolder;
import com.tencent.qcloud.timchat.R;

import q.rorbin.badgeview.Badge;
import q.rorbin.badgeview.QBadgeView;

public class ConversationViewHolder extends BaseViewHolder {
    Badge badge;

    public ConversationViewHolder(final View view) {
        super(view);
        badge = new QBadgeView(view.getContext()).bindTarget(view.findViewById(R.id.unread_num));
        badge.setBadgeGravity(Gravity.CENTER);
        badge.setBadgeTextSize(12, true);
        badge.setBadgePadding(6, true);
    }
}
