package com.tencent.qcloud.timchat.model;

/**
 * 聊天用户信息
 */
public interface ImChatUser {
    String getImUserName();

    String getImUserPsw();

    String getImNickName();

    String getImUserAvatar();

    String getImPhone();
}
