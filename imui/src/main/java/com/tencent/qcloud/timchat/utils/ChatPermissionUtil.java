package com.tencent.qcloud.timchat.utils;

import android.net.Uri;
import android.os.Build;
import android.support.v4.content.FileProvider;

import com.tencent.qalsdk.util.BaseApplication;
import com.tencent.qcloud.timchat.ImInit;

import java.io.File;

/**
 * Android 6.0+ 权限相关
 * Created by wc on 2017/10/26.
 */
public class ChatPermissionUtil {
    public static Uri getUri(File file) {
        Uri uri = Uri.fromFile(file);
        if (Build.VERSION.SDK_INT > 23) {
            uri = FileProvider.getUriForFile(ImInit.getContext().getApplicationContext(), ImInit.getContext().getPackageName() + ".fileProvider", file);
        }
        return uri;
    }
}
