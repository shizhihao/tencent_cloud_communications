package com.tencent.qcloud.timchat.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.tencent.qcloud.timchat.BaseActivity;
import com.tencent.qcloud.timchat.R;
import com.tencent.qcloud.timchat.utils.FileUtil;

public class ImageViewActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view);
        findViewById(R.id.root).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        String file = getIntent().getStringExtra("filename");
        Glide.with(this)
                .load(FileUtil.getCacheFilePath(file))
                .into((ImageView) findViewById(R.id.image));
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.alpha_gone, R.anim.alpha_gone);
    }
}
