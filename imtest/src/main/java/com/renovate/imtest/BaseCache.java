package com.renovate.imtest;


/**
 * Created by Administrator on 2016/1/5.
 */
public abstract class BaseCache<T> {
    public DbUtil dbUtil = DbUtil.getInstance();

    protected abstract Class<T> getThisClass();

    public abstract void clearCache();
}