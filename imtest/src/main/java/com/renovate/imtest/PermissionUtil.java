package com.renovate.imtest;

import android.net.Uri;
import android.os.Build;
import android.support.v4.content.FileProvider;

import java.io.File;

/**
 * Android 6.0+ 权限相关
 * Created by wc on 2017/10/26.
 */
public class PermissionUtil {
    public static Uri getUri(File file) {
        Uri uri = Uri.fromFile(file);
        if (Build.VERSION.SDK_INT > 23) {
            uri = FileProvider.getUriForFile(BaseApplication.Companion.getInstance().getApplicationContext(), BuildConfig.APPLICATION_ID + ".fileProvider", file);
        }
        return uri;
    }
}
