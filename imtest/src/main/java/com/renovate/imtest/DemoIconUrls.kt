package com.renovate.imtest

object DemoIconUrls {
    val demoUrls = arrayListOf(
            "http://111.231.79.158/demo_01.jpg",
            "http://111.231.79.158/demo_02.jpg",
            "http://111.231.79.158/demo_03.jpg",
            "http://111.231.79.158/demo_04.jpg",
            "http://111.231.79.158/demo_05.jpg")
    val demoIconUrls = arrayListOf(
            "http://111.231.79.158/demo_icon_01.jpg",
            "http://111.231.79.158/demo_icon_02.jpg",
            "http://111.231.79.158/demo_icon_03.jpg",
            "http://111.231.79.158/demo_icon_04.jpg",
            "http://111.231.79.158/demo_icon_05.jpg",
            "http://111.231.79.158/demo_icon_06.jpg",
            "http://111.231.79.158/demo_icon_07.jpg",
            "http://111.231.79.158/demo_icon_08.jpg",
            "http://111.231.79.158/demo_icon_09.jpg")
    val storeLogoUrls = arrayListOf(
            "http://111.231.79.158/store_logo_01.jpg",
            "http://111.231.79.158/store_logo_02.jpg",
            "http://111.231.79.158/store_logo_03.jpg",
            "http://111.231.79.158/store_logo_04.jpg",
            "http://111.231.79.158/store_logo_05.jpg")
    val companyLogoUrls = arrayListOf(
            "http://111.231.79.158/company_logo_01.jpg",
            "http://111.231.79.158/company_logo_02.jpg",
            "http://111.231.79.158/company_logo_03.jpg",
            "http://111.231.79.158/company_logo_04.jpg",
            "http://111.231.79.158/company_logo_05.jpg",
            "http://111.231.79.158/company_logo_06.jpg")
    val materialIconUrls = arrayListOf(
            "http://111.231.79.158/material_icon_01.png",
            "http://111.231.79.158/material_icon_02.png",
            "http://111.231.79.158/material_icon_03.png",
            "http://111.231.79.158/material_icon_04.png",
            "http://111.231.79.158/material_icon_05.png",
            "http://111.231.79.158/material_icon_06.png",
            "http://111.231.79.158/material_icon_07.png",
            "http://111.231.79.158/material_icon_08.png",
            "http://111.231.79.158/material_icon_09.png"
    )


    private fun getUrl(urls: ArrayList<String>, position: Int): String {
        val index = position % urls.size
        return urls[index]
    }

    fun getDemoUrls(position: Int): String {
        return getUrl(demoUrls, position)
    }

    fun getDemoIconUrls(position: Int): String {
        return getUrl(demoIconUrls, position)
    }

    fun getStoreLogoUrls(position: Int): String {
        return getUrl(storeLogoUrls, position)
    }

    fun getCompanyLogoUrls(position: Int): String {
        return getUrl(companyLogoUrls, position)
    }

    fun getMaterialIconUrls(position: Int): String {
        return getUrl(materialIconUrls, position)
    }
}
