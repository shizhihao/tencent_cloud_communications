package com.renovate.imtest;

import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.tencent.imsdk.TIMManager;

import org.xutils.ex.DbException;

/**
 * Created by Administrator on 2017/1/11.
 */
public class ChatUserCache extends BaseListCache<ChatUser> {
    private static ChatUserCache instance;
    private ChatUser loginUser;

    private ChatUserCache() {
        super();
    }

    public static synchronized ChatUserCache getInstance() {
        if (instance == null)
            instance = new ChatUserCache();
        return instance;
    }

    public ChatUser getStoreUser(int smiid, DataResponse response) {
        return getStoreUser(-1, smiid, response);
    }

    public ChatUser getStoreUser(int guid, int smiid, DataResponse response) {
        if (TextUtils.isEmpty(TIMManager.getInstance().getLoginUser())) {
//            ChatHelper.getInstance().chatUser(null, null);
            if (response != null) {
                response.onError("链接中断，重新登录");
            }
            return null;
        }
        ChatUser chatUser = null;
        try {
            if (guid <= 0)
                chatUser = dbUtil.getDbManager().selector(getThisClass()).where("smiid", "=", smiid).orderBy("guid").findFirst();
            else
                chatUser = dbUtil.getDbManager().selector(getThisClass()).where("cId", "=", guid).findFirst();
        } catch (DbException e) {
            e.printStackTrace();
        }
        if (response != null) {
            if (chatUser == null) {
                requestStoreUser(null, guid < 0 ? null : String.valueOf(guid), smiid < 0 ? null : String.valueOf(smiid), response);
            } else {
                response.onSuccess();
                response.onResponse(chatUser);
                requestStoreUser(null, guid < 0 ? null : String.valueOf(guid), smiid < 0 ? null : String.valueOf(smiid), null);
            }
        }
        return chatUser;
    }

    private void requestStoreUser(String userName, String guid, String smiid, final DataResponse dataResponse) {
    }

    public ChatUser getLoginUser() {
        if (loginUser == null || TextUtils.isEmpty(loginUser.getChatUserName())) {
        }
        return loginUser;
    }

    public void setLoginUser(ChatUser loginUser) {
        this.loginUser = loginUser;
        addToCacheList(loginUser);
    }

    public ChatUser getUser(int guid) {
        for (int i = 0; i < mList.size(); i++) {
            if (guid == mList.get(i).getGuid())
                return mList.get(i);
        }
        requestStoreUser(null, guid <= 0 ? null : String.valueOf(guid), null, null);
        return null;
    }

    public ChatUser getUser(String userName) {
        for (int i = 0; i < mList.size(); i++) {
            if (userName.equals(mList.get(i).getChatUserName()))
                return mList.get(i);
        }
        requestStoreUser(userName, null, null, null);
        return null;
    }

    public ChatUser getUser(String userName, DataResponse dataResponse) {
        for (int i = 0; i < mList.size(); i++) {
            if (userName.equals(mList.get(i).getChatUserName()))
                return mList.get(i);
        }
        requestStoreUser(userName, null, null, dataResponse);
        return null;
    }

    @Override
    public void addToCacheList(ChatUser entry) {
        if (entry == null)
            return;
        if (mList.contains(entry)) {
            mList.remove(entry);
        }
        mList.add(entry);
        dbUtil.saveOrUpdate(entry);
    }

    @Override
    protected Class<ChatUser> getThisClass() {
        return ChatUser.class;
    }
}
