//package com.renovate.imtest;
//
//import android.app.Application;
//import android.content.Context;
//import android.content.Intent;
//import android.os.Build;
//import android.text.TextUtils;
//import android.util.Log;
//import android.widget.TextView;
//
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.tencent.imsdk.TIMCallBack;
//import com.tencent.imsdk.TIMConversation;
//import com.tencent.imsdk.TIMConversationType;
//import com.tencent.imsdk.TIMLogLevel;
//import com.tencent.imsdk.TIMManager;
//import com.tencent.imsdk.TIMMessage;
//import com.tencent.imsdk.TIMMessageListener;
//import com.tencent.imsdk.TIMOfflinePushSettings;
//import com.tencent.imsdk.TIMOfflinePushToken;
//import com.tencent.imsdk.TIMSdkConfig;
//import com.tencent.imsdk.TIMUserConfig;
//import com.tencent.imsdk.TIMUserStatusListener;
//import com.tencent.imsdk.ext.message.TIMConversationExt;
//import com.tencent.imsdk.ext.message.TIMManagerExt;
//import com.tencent.qcloud.presentation.event.MessageEvent;
//import com.tencent.qcloud.presentation.event.RefreshEvent;
//import com.tencent.qcloud.timchat.ImInit;
//import com.tencent.qcloud.timchat.ui.ChatActivity;
//import com.tencent.qcloud.timchat.ui.HomeActivity;
//import com.tencent.qcloud.timchat.utils.PushUtil;
//
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import tencent.tls.platform.TLSHelper;
//
///**
// * Created by szh on 2018/2/2.
// */
//
//public class ChatHelper {
//    private boolean loginFirst = false;
//
//    private Map<String, TIMCallBack> callBackMap = new HashMap<>();
//
//    private static class ChatHelperInstance {
//        private static final ChatHelper chatHelper = new ChatHelper();
//    }
//
//    public static ChatHelper getInstance() {
//        return ChatHelperInstance.chatHelper;
//    }
//
//    public void initIm(Application application) {
//        ImInit.init(application, R.drawable.logo);
//        ImInit.setUserProfileProvider(new ImInit.UserProfileProvider() {
//            @Override
//            public String getUserNikeName(String username) {
//                try {
//                    ChatUser user = ChatUserCache.getInstance().getUser(username);
//                    if (user == null) {
//                        return getSimpleNickName(username);
//                    }
//                    return ChatUserCache.getInstance().getUser(username).getNickName();
//                } catch (Exception e) {
//                    return null;
//                }
//            }
//
//            @Override
//            public TimUserProfile getUserProfile(String username) {
//                try {
//                    TimUserProfile timUserProfile = new TimUserProfile();
//                    ChatUser user = ChatUserCache.getInstance().getUser(username);
//                    if (user == null) {
//                        timUserProfile.nickName = getSimpleNickName(username);
//                    } else {
//                        timUserProfile.nickName = user.getNickName();
//                        timUserProfile.faceUrl = user.getAvatar();
//                        timUserProfile.registerPhone = user.getPhone();
//                    }
//                    return timUserProfile;
//                } catch (Exception e) {
//                    return null;
//                }
//            }
//
//            @Override
//            public void getUserProfile(String username, final TIMCallBack timCallBack) {
//                TimUserProfile timUserProfile = new TimUserProfile();
//                ChatUser user = ChatUserCache.getInstance().getUser(username, new BaseListCache.DataResponse() {
//                    @Override
//                    public void onSuccess() {
//                    }
//
//                    @Override
//                    public void onResponse(Object object) {
//                        timCallBack.onSuccess();
//                    }
//
//                    @Override
//                    public void onError(Object error) {
//
//                    }
//                });
//                if (user == null) {
//                    timUserProfile.nickName = getSimpleNickName(username);
//                    timCallBack.onSuccess();
//                } else {
//                    timUserProfile.nickName = user.getNickName();
//                    timUserProfile.faceUrl = user.getAvatar();
//                    timUserProfile.registerPhone = user.getPhone();
//                    timCallBack.onSuccess();
//                }
//            }
//        });
//        TIMSdkConfig config = new TIMSdkConfig(1400061752);
//        config.enableLogPrint(true).setLogLevel(TIMLogLevel.OFF)
//                .enableCrashReport(false);
//        TIMManager.getInstance().init(application, config);
//
//        TLSHelper.getInstance().init(application, 1400061752);
//
//        TIMManager.getInstance().addMessageListener(new TIMMessageListener() {
//            @Override
//            public boolean onNewMessages(List<TIMMessage> list) {
//                EaseMessageReceive messageReceive = new EaseMessageReceive();
//                messageReceive.num = getAllUnreadMessageNum();
//                BaseApplication.getDefault().post(messageReceive);
//                return false;
//            }
//        });
//
//        if (!UserCache.getInstance().isLogin())
//            return;
//        if (TextUtils.isEmpty(TIMManager.getInstance().getLoginUser())) {
//            ChatUser loginUser = ChatUserCache.getInstance().getLoginUser();
//            loginIm(loginUser.getChatUserName(), loginUser.getChatPassword());
//        } else {
//            initPushAbout();
//        }
//    }
//
//    private void initPushAbout() {
//        TIMOfflinePushSettings settings = new TIMOfflinePushSettings();
//        //开启离线推送
//        settings.setEnabled(true);
//        TIMManager.getInstance().setOfflinePushSettings(settings);
//        setPushToken();
//
//        //初始化消息监听
//        MessageEvent.getInstance();
//        //初始化程序后台后消息推送
//        PushUtil.getInstance();
//
//        EaseMessageReceive event = new EaseMessageReceive();
//        event.num = getAllUnreadMessageNum();
//        BaseApplication.getDefault().post(event);
//    }
//
//    private void setPushToken() {
//        TIMOfflinePushToken token = null;
//        Application application = BaseApplication.getInstance();
//        if (MiPushRegistar.checkDevice(application)) {
//            token = new TIMOfflinePushToken(3273, MiPushClient.getRegId(application));
//        } else if ("huawei".equalsIgnoreCase(Build.BRAND) || "honor".equalsIgnoreCase(Build.BRAND)) {
//            String pushToken = SPUtils.getString(Constants.SP_HUAWEI_PUSH_TOKEN);
//            if (TextUtils.isEmpty(pushToken)) {
//                HuaWeiReceiver.getToken(application);
//            } else {
//                token = new TIMOfflinePushToken(3761, pushToken);
//            }
//        } else if (MzSystemUtils.isBrandMeizu()) {
//            token = new TIMOfflinePushToken(3762, PushManager.getPushId(application));
//        }
//        if (token != null)
//            TIMManager.getInstance().setOfflinePushToken(token, new TIMCallBack() {
//                @Override
//                public void onError(int i, String s) {
//                    Log.i("Chat", "IMUI MiPush error:" + s);
//                }
//
//                @Override
//                public void onSuccess() {
//                    Log.i("Chat", "IMUI MiPush Success");
//                }
//            });
//    }
//
//    public String getSimpleNickName(String username) {
//        StringBuilder nikeNameBuilder = new StringBuilder();
//        if (username.startsWith("ehu")) {
//            nikeNameBuilder.append("易乎用户");
//        } else if (username.startsWith("ehm")) {
//            nikeNameBuilder.append("易乎商店");
//        } else if (username.startsWith("ehc")) {
//            nikeNameBuilder.append("易乎小店");
//        }
//        if (username.length() > 13) {
//            nikeNameBuilder.append(username.substring(10, 14));
//        }
//        return nikeNameBuilder.toString();
//    }
//
//    public void comeChatHome(final Context context) {
//        if (UserCache.checkIsLogin()) {
//            chatUser(ChatUserCache.getInstance().getLoginUser(), new TIMCallBack() {
//                @Override
//                public void onError(int i, String s) {
//                }
//
//                @Override
//                public void onSuccess() {
//                    context.startActivity(new Intent(context, HomeActivity.class));
//                }
//            });
//        }
//    }
//
//    public long getUnreadMessageNum(int smiid) {
//        if (smiid == 0)
//            return 0;
//        ChatUser user = ChatUserCache.getInstance().getStoreUser(smiid, null);
//        if (user == null)
//            return 0;
//        //获取会话扩展实例
//        TIMConversation con = TIMManager.getInstance().getConversation(TIMConversationType.C2C, user.getChatUserName());
//        TIMConversationExt conExt = new TIMConversationExt(con);
//        //获取会话未读数
//        long num = conExt.getUnreadMessageNum();
//        return num;
//    }
//
//    public void setUnreadMessageNum(int smiid, final TextView chatText) {
//        if (chatText == null)
//            return;
//        ChatUserCache.getInstance().getStoreUser(smiid,
//                new BaseListCache.DataResponse() {
//                    @Override
//                    public void onSuccess() {
//                    }
//
//                    @Override
//                    public void onResponse(Object object) {
//                        //获取会话扩展实例
//                        TIMConversation con = TIMManager.getInstance().getConversation(TIMConversationType.C2C, ((ChatUser) object).getChatUserName());
//                        TIMConversationExt conExt = new TIMConversationExt(con);
//                        //获取会话未读数
//                        chatText.setText(String.valueOf(conExt.getUnreadMessageNum()));
//                    }
//
//                    @Override
//                    public void onError(Object error) {
//                        chatText.setText("0");
//                    }
//                });
//    }
//
//    public long getAllUnreadMessageNum() {
//        List<TIMConversation> list = TIMManagerExt.getInstance().getConversationList();
//        long num = 0;
//        for (TIMConversation conversation : list) {
//            if (conversation.getType() == TIMConversationType.System) continue;
//            TIMConversationExt conExt = new TIMConversationExt(conversation);
//            num += conExt.getUnreadMessageNum();
//        }
//        return num;
//    }
//
//    public void chatUser(String user, int guId, final TIMCallBack timCallBack) {
//        if (TextUtils.isEmpty(user)) {
//            ChatUser chatUser = ChatUserCache.getInstance().getUser(guId);
//            if (chatUser != null) {
//                chatUser(chatUser, timCallBack);
//            } else {
//                AsyncHttp.getInstance().addRequest(new GetChatInfoRequest(
//                        user, guId < 0 ? null : String.valueOf(guId), null,
//                        new Response.Listener<UserChatResult>() {
//                            @Override
//                            public void onResponse(UserChatResult response) {
//                                chatUser(response.data.get(0), timCallBack);
//                            }
//                        },
//                        new CodeErrorListener() {
//                            @Override
//                            public void onErrorResponse(VolleyError error) {
//                                if (timCallBack != null) {
//                                    timCallBack.onError(0, null);
//                                }
//                            }
//                        }.showCodeConstant(false).setComeLogin(false)));
//            }
//        } else {
//            chatUser(new ChatUser(user), timCallBack);
//        }
//    }
//
//    public void chatUserToChatActivity(final Context context, int smiid, final TIMCallBack timCallBack) {
//        if (smiid <= 0) {
//            if (timCallBack != null)
//                timCallBack.onError(0, "无效的店铺信息");
//            return;
//        }
//        ChatUserCache.getInstance().getStoreUser(smiid,
//                new BaseListCache.DataResponse() {
//                    @Override
//                    public void onSuccess() {
//                    }
//
//                    @Override
//                    public void onResponse(final Object object) {
//                        chatUser((ChatUser) object, new TIMCallBack() {
//                            @Override
//                            public void onError(int i, String s) {
//
//                            }
//
//                            @Override
//                            public void onSuccess() {
//                                ChatActivity.navToChat(context, ((ChatUser) object).getChatUserName(), TIMConversationType.C2C);
//                                if (timCallBack != null)
//                                    timCallBack.onSuccess();
//                                BaseApplication.getDefault().post(new EaseMessageReceive());
//                            }
//                        });
//                    }
//
//                    @Override
//                    public void onError(Object error) {
//                        if (error != null) {
//                            ToastUtils.showShortToast(error.toString());
//                            if (timCallBack != null)
//                                timCallBack.onError(0, error.toString());
//                        }
//                    }
//                });
//    }
//
//    public void chatUser(ChatUser user, TIMCallBack timCallBack) {
//        if (user != null && TextUtils.isEmpty(user.getChatUserName())) {
//            Log.e("IM", "无效的聊天用户");
//            if (timCallBack != null)
//                timCallBack.onError(0, "");
//            return;
//        }
//        ChatUser loginUser = ChatUserCache.getInstance().getLoginUser();
//        if (TextUtils.isEmpty(TIMManager.getInstance().getLoginUser())) {
//            if (user != null)
//                callBackMap.put(user.getChatUserName(), timCallBack);
//
//            loginFirst = true;
//            loginIm(loginUser.getChatUserName(), loginUser.getChatPassword());
//        } else if (timCallBack != null) {
//            timCallBack.onSuccess();
//        }
//    }
//
//    private void loginIm(final String userName, String passWord) {
//        TIMUserConfig userConfig = new TIMUserConfig();
//        userConfig.setUserStatusListener(
//                new TIMUserStatusListener() {
//                    @Override
//                    public void onForceOffline() {
//                        Log.e("IM", "账号异地登录");
//                    }
//
//                    @Override
//                    public void onUserSigExpired() {
////                        ToastUtils.showShortToast(com.ehu.tencentim.R.string.tls_expire);
//                    }
//                });
//        //设置刷新监听
//        RefreshEvent.getInstance().init(userConfig);
//        userConfig = MessageEvent.getInstance().init(userConfig);
//        TIMManager.getInstance().setUserConfig(userConfig);
//
//        TIMManager.getInstance().login(userName, passWord,
//                new TIMCallBack() {
//                    @Override
//                    public void onError(int i, String s) {
//                        TIMCallBack timCallBack = callBackMap.get(userName);
//
//                        switch (i) {
//                            case 6208:
//                                //离线状态下被其他终端踢下线
//                                ToastUtils.showShortToast(com.ehu.tencentim.R.string.kick_logout);
//                                if (timCallBack != null) {
//                                    timCallBack.onError(i, s);
//                                    callBackMap.remove(userName);
//                                }
//                                break;
//                            case 6200:
//                                Log.e("IM", BaseApplication.getInstance().getString(com.ehu.tencentim.R.string.login_error_timeout));
//                                if (timCallBack != null) {
//                                    timCallBack.onError(i, s);
//                                    callBackMap.remove(userName);
//                                }
//                                break;
//                            default:
//                                if (!loginFirst)
//                                    Log.e("IM", "i= " + i + "msg= " + s);
//                                loginError();
//                                break;
//                        }
//                    }
//
//                    @Override
//                    public void onSuccess() {
//                        initPushAbout();
//
//                        TIMCallBack timCallBack = callBackMap.get(userName);
//                        if (timCallBack != null) {
//                            timCallBack.onSuccess();
//                            callBackMap.remove(userName);
//                        }
//                    }
//                });
//    }
//
//    private void loginError() {
//        if (loginFirst) {
//            loginFirst = false;
//            AsyncHttp.getInstance().addRequest(new ResetChatInfoRequest(
//                    new Response.Listener<ChatInfoResult>() {
//                        @Override
//                        public void onResponse(ChatInfoResult response) {
//                            Log.e("IM", "聊天状态失效，为您重新登录成功");
//                            loginIm(response.data.getChatUserName(), response.data.getChatPassword());
//                        }
//                    }, new CodeErrorListener().showCodeConstant(false).setComeLogin(false)));
//        }
//    }
//
//    private void getUserChatInfo(String user, int guId, final TIMCallBack timCallBack) {
//        AsyncHttp.getInstance().addRequest(new GetUserChatRequest(
//                user, guId,
//                new Response.Listener<ChatInfoResult>() {
//                    @Override
//                    public void onResponse(ChatInfoResult response) {
//                        if (timCallBack != null)
//                            timCallBack.onSuccess();
//                    }
//                }, new CodeErrorListener().showCodeConstant(false).setComeLogin(false)));
//    }
//
//    public void loginOut() {
//        ChatUserCache.getInstance().clearCache();
//        MessageEvent.getInstance().clear();
//        TLSHelper.getInstance().clearUserInfo(ChatUserCache.getInstance().getLoginUser().getChatUserName());
//        TIMManager.getInstance().logout(null);
//    }
//}
