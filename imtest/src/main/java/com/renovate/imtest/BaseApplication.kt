package com.renovate.imtest

import android.app.Application

import com.tencent.qcloud.timchat.ImInit

class BaseApplication : Application() {
    companion object {
        lateinit var instance: BaseApplication
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        ImInit.init(this)
        ImInit.getInstance().setUserInfoProvider(MyUserInfoProvider(this))
        ImInit.getInstance().setNewChatActivity("com.renovate.imtest.ChatWhitActivity")
    }
}
