package com.renovate.imtest;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.tencent.qcloud.timchat.model.ImChatUser;

import org.xutils.db.annotation.Column;
import org.xutils.db.annotation.Table;

/**
 * Created by Administrator on 2017/1/10.
 */
@Table(name = "chat_user")
public class ChatUser implements Parcelable, ImChatUser {
    @Column(name = "cId", isId = true, autoGen = false)
    private int guid;
    @Column(name = "chatUserName")
    private String chatUserName;
    @Column(name = "chatPassword")
    private String chatPassword;
    @Column(name = "nickname")
    private String nickName;
    @Column(name = "avatar")
    private String avatar;
    @Column(name = "phone")
    private String phone;
    @Column(name = "smiid")
    private String smiid;

    public int getGuid() {
        return guid;
    }

    public void setGuid(int guid) {
        this.guid = guid;
    }

    public String getChatUserName() {
        return chatUserName;
    }

    public void setChatUserName(String chatUserName) {
        this.chatUserName = chatUserName;
    }

    public String getChatPassword() {
        return chatPassword;
    }

    public void setChatPassword(String chatPassword) {
        this.chatPassword = chatPassword;
    }

    public String getNickName() {
        if (!TextUtils.isEmpty(chatUserName) && TextUtils.isEmpty(nickName)) {
            StringBuilder nikeNameBuilder = new StringBuilder();
            if (chatUserName.startsWith("ehu")) {
                nikeNameBuilder.append("易乎用户");
            } else if (chatUserName.startsWith("ehm")) {
                nikeNameBuilder.append("易乎商店");
            } else if (chatUserName.startsWith("ehc")) {
                nikeNameBuilder.append("易乎小店");
            }
            if (TextUtils.isEmpty(getPhone()) && chatUserName.length() > 13) {
                nikeNameBuilder.append(chatUserName.substring(9, 13));
            } else if (getPhone().length() >= 4) {
                nikeNameBuilder.append(getPhone().substring(getPhone().length() - 4, getPhone().length()));
            }
            setNickName(nikeNameBuilder.toString());
        }
        return nickName;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ChatUser) {
            if (guid != 0 && ((ChatUser) obj).guid != 0) {
                return guid == ((ChatUser) obj).guid;
            } else if (!TextUtils.isEmpty(chatUserName)) {
                return chatUserName.equals(((ChatUser) obj).chatUserName);
            }
        }
        return super.equals(obj);
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public ChatUser(String chatUserName) {
        this.chatUserName = chatUserName;
    }

    public ChatUser() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.guid);
        dest.writeString(this.chatUserName);
        dest.writeString(this.chatPassword);
        dest.writeString(this.nickName);
        dest.writeString(this.avatar);
        dest.writeString(this.phone);
        dest.writeString(this.smiid);
    }

    protected ChatUser(Parcel in) {
        this.guid = in.readInt();
        this.chatUserName = in.readString();
        this.chatPassword = in.readString();
        this.nickName = in.readString();
        this.avatar = in.readString();
        this.phone = in.readString();
        this.smiid = in.readString();
    }

    public static final Creator<ChatUser> CREATOR = new Creator<ChatUser>() {
        @Override
        public ChatUser createFromParcel(Parcel source) {
            return new ChatUser(source);
        }

        @Override
        public ChatUser[] newArray(int size) {
            return new ChatUser[size];
        }
    };

    @Override
    public String getImUserName() {
        return chatUserName;
    }

    @Override
    public String getImUserPsw() {
        return chatPassword;
    }

    @Override
    public String getImNickName() {
        return nickName;
    }

    @Override
    public String getImUserAvatar() {
        return avatar;
    }

    @Override
    public String getImPhone() {
        return phone;
    }
}