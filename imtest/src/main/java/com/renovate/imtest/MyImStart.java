package com.renovate.imtest;

import android.util.Log;

import com.tencent.qcloud.timchat.ImStart;

public class MyImStart extends ImStart {
    @Override
    public void onSuccess() {
        super.onSuccess();

        Log.d("MyImStart", "登录成功");
    }

    @Override
    public void onError(int i, String s) {
        super.onError(i, s);
    }
}
