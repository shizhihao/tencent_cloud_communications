package com.renovate.imtest

import android.content.Context
import android.os.Handler
import com.tencent.qcloud.timchat.model.ImChatUser
import com.tencent.qcloud.timchat.model.ImRequestListener
import com.tencent.qcloud.timchat.model.UserInfoProvider

import java.util.ArrayList

class MyUserInfoProvider(private val context: Context) : UserInfoProvider() {
    private val handler: Handler = Handler()

    companion object {
        var index = 0
    }

    private fun getImageUrl(): String {
        return DemoIconUrls.getDemoIconUrls(index)
    }

    override fun getUserInfo(userName: ArrayList<String>?, listener: ImRequestListener?) {
        handler.postDelayed({
            index = 4
            requestSuccess(listener)
            listener?.success(arrayListOf(ChatUser().apply {
                nickName = "变化$userName"
                avatar = getImageUrl()
            }, ChatUser().apply {
                nickName = "变化$userName"
                avatar = getImageUrl()
            }))
        }, 2000)
    }

    override fun getUserInfo(userName: String?): ImChatUser? {
        if (USER_SELF == userName) {
            return ChatUser().apply {
                nickName = "我我我"
                avatar = DemoIconUrls.getDemoIconUrls(2)
                chatUserName = "10"
                chatPassword = "eJxlz9FOgzAUBuB7nqLpLUZbsIOY7IIZzUg6JxtG2U2D9MAahXWsgmh8dydqbOK5-f6T-5x3ByGEU74*zYti99IYYQYNGF0gTPDJH2qtpMiN8Fv5D*FVqxZEXhpoR6SMMY8QO6MkNEaV6jdh20E*ibHgm86Pm8EkZIEdUdWIi6vkMp6tNXDuyr13u-Uy2hUMsm0yT*Zvg4nDqsseq7t9uJyVA4lUlMYuX10-n4U6MSu3h7rebHi0IMsuupH5Q9Lfu2nQc-BVP51alUbV8PONz9jkeBK1tIP2oHbNGPAIZdTzyddg58P5BEtHW2Q_"
//                chatUserName = "aabb"
//                chatPassword = "eJxlj11PgzAARd-5FQ2vGin9YMzEhzlQISOu29yCL02BbqtkgNAtLMb-ruISm3hfz7m5uR8WAMBezZY3Is-rY6W5PjfSBrfAhvb1H2waVXChOW6Lf1D2jWolF1st2wG6lFIEoemoQlZabdXFECLLDNoVJR8mfuvku*sRNB6bitoNMAlfphELFksnJknSH94SieYo82gI1zJ*n7A09bo*rVK1L-FM*CWLduGTM82C6Exyto6PLWSPD1daBpN76IxW4bO-2NSvrNrs5x25Mya1OsjLH0yx744gMehJtp2qq0FA0KUuwvAntvVpfQHkRlsm"
            }
        }

        return ChatUser().apply {
            nickName = "变化$userName"
            avatar = getImageUrl()
            chatUserName = userName
        }
    }
}
