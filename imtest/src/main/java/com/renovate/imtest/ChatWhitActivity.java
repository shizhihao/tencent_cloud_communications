package com.renovate.imtest;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.tencent.qcloud.timchat.ui.ChatFragment;


public class ChatWhitActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_my_caht);

        getSupportFragmentManager().beginTransaction().add(R.id.content, new ChatFragment()).commit();
    }
}
